from random import randint
from rich import print
from .tile import Tile
from math import sqrt


# Define constants
matrix = list[list[Tile]]
root_two = sqrt(2)


class Field:
	def __init__(self, width: int, height: int):
		self.width, self.height = width, height

		self.set_goal(width-1, height-1)

		self.initialize_field()

		self.visited = []
		self.to_visit = []
		self.path = [(width-1, height-1)]

	def initialize_field(self):
		"""initializes the field with the given width and height
		and populates it with 30% wall tiles and 70% ordinary tiles"""

		def get_tile(i: int, j: int):
			if randint(0, 100) < 35:
				return None
			else:
				# h cost for each tile which is the distance
				# from the tiles location to the goal
				h = sqrt((i - self.goal[0])**2 + (j - self.goal[1])**2)
				return Tile(h)

		self.field = [[get_tile(i, j) for j in range(self.width)]
					  for i in range(self.height)]

		self.field[self.goal[0]][self.goal[1]] = Tile(0)

	def set_goal(self, x: int, y: int):
		if 0 < x < self.width:
			if 0 < y < self.height:
				self.goal = (x, y)
				# self.path = [(x, y)]
			else:
				raise ValueError(
					f'y ({y}) is out of bounds for field height ({self.height})')
		else:
			raise ValueError(
				f'x ({x}) is out of bounds for field width ({self.width})')

	def get_neighbors(self, x: int, y: int) -> list[(int, int)]:
		neighbors = []

		for i in range(-1, 2):
			for j in range(-1, 2):
				if not (i == 0 and j == 0) and 0 <= x+i < self.width and 0 <= y+j < self.height:
					if self.field[x+i][y+j] is not None and (x+i, y+j) not in self.visited:
						neighbors.append((x+i, y+j))

		return neighbors

	def add_to_visit(self, pos: (int, int), origin: (int, int)):

		g = self.field[origin[0]][origin[1]].g + \
			sqrt((pos[0]-origin[0])**2 + (pos[1]-origin[1])**2)

		if pos not in self.to_visit:
			self.field[pos[0]][pos[1]].set_g(g, origin)

			self.to_visit.append((pos[0], pos[1]))
		else:
			if g < self.field[pos[0]][pos[1]].g:
				self.field[pos[0]][pos[1]].set_g(g, origin)


	def pathfind(self, x: int, y: int):
		"""x, y - location from which which pathfindind should start"""
		self.visited = [(x, y)]
		self.field[x][y] = Tile(
			sqrt((x - self.goal[0])**2 + (y - self.goal[1])**2))
		self.field[x][y].set_g(0, (x, y))

		for neighbor in self.get_neighbors(x, y):
			self.add_to_visit(neighbor, (x, y))

		while self.goal not in self.visited and len(self.to_visit) > 0:
			self.to_visit.sort(key=lambda a: self.field[a[0]][a[1]].get_cost())

			c = self.to_visit.pop(0)

			self.visited.append(c)

			for neighbor in self.get_neighbors(c[0], c[1]):
				self.add_to_visit(neighbor, (c[0], c[1]))

			# Uncomment this to do live visualization
			self.print_field()

		# Backtrack the shortest path from the goal to the start

		while self.goal in self.visited and (x, y) not in self.path:
			c = self.field[self.path[-1][0]][self.path[-1][1]].shortest_ingress_direction

			self.path.append(c)


	def print_field(self):
		"""Prints the field"""
		string = ""
		for j in range(self.height):
			for i in range(self.width):
				color = ""
				if (i, j) == self.goal:
					# if this tile is the goal
					color = "blue"

				elif (i, j) in self.path:
					# if this is a tile that's just to be checked
					color = "blue"

				elif (i, j) in self.visited:
					# if this is a tile that's just to be checked
					color = "green"

				elif (i, j) in self.to_visit:
					# if this is a tile that's just to be checked
					color = "red"

				elif self.field[i][j] is None:
					# if this is a wall
					color = "bold"
				else:
					# if this is a tile
					color = "black"
				string += f'[{color}]██[/{color}]'
			string += "\n"

		print(string)
