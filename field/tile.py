class Tile:
	def __init__(self, h: float):
		self.h = h
		self.g = None
		self.shortest_ingress_direction = None

	def set_g(self, g: float, origin: int):
		self.g = g
		self.shortest_ingress_direction = origin

		# print(f'g - {self.g}')

	def get_cost(self) -> float:
		if self.g is None:
			raise ValueError('"g" cost not yet set')
		return self.g + self.h
