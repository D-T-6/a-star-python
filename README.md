# A*

A super simple implementation of the A* path-finding algorithm done in less than an hour. uses [rich](https://github.com/willmcgugan/rich) for printing colorful text to the terminal.

### How to run

do:
```
python3 .
```
in the root folder

### Output 

![](images/50%25-walls.png)
