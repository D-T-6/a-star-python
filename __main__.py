from field import Field


def main():
    green_field = Field(32, 32)

    green_field.pathfind(0, 0)

    green_field.print_field()


if __name__ == "__main__":
    main()
